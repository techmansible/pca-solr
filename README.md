Prudential Solr and Zookeeper installation
Ansible playbook to Install prerequisties, download the binary of Solr and Zookeeper, untar and install it.

Variables that can be passed dynamically during playbook execution
run_as - User to run the playbook 
zookeeper_user_name - Zookeeper app username 
zookeeper_group_name - Zookeeper app group 
solr_user_name - Solr app username
solr_group_name - Solr app  group name 
zookeeper_hostname - Zookeeper server hostname

Running Playbook Example
ansible-playbook -i inventories install.yml 

group_vars
Update the server IPs in respective group_vars file. Example:

zookeeper_hostname - Zookeeper server hostname

Inventory
Update hostnames in inventories/hosts file. Example:

[zookeeper] 
ahklifelddar004

[solr] 
ahklifelddar005

Roles:
Below are the roles used in this playbook

zookeeper-install -- Download and Untars the binary of Zookeeper and install Zookeeper.

Install Zookeeper Role
---------------------------
1. Install Java
2. Download the tar file from Artifactory
3. Untar the source file
4. Copy zoo.cfg file to Zookeeper home directory
5. Start the Zookeeper service

solr-install -- Download and Untars the binary of Solr and install Solr.

Install Solr Role:
------------------
1. Install Java
2. Download tar file from artifactory
3. Untar the source file
4. Start the solr Service
5. Downgrade the solr configuration
6. Upload new Scehema file to solr and Zookeeper

Run the following playbook to uninstall Zookeeper and Solr
-------------------------------------------------
### uninstall.yml -- Uninstall the Zookeeper and Solr.

ansible-playbook -i inventories uninstall.yml

